## Tecnologias Utilizadas
* JDK 1.8+
* Docker
* Docker Compose
* Framework Java Quarkus
* MYSQL 5.7

## Rodando a Aplicação

## Mysql 
docker-compose up -d

* Caso queira rodar diretamente sem o compose do docker
docker run -p 3386:3306 --name lyncas-test-mysql -e MYSQL_DATABASE=books -e MYSQL_ROOT_PASSWORD=root -d mysql:latest

## Subir aplicação
* ./mvnw compile quarkus:dev

## Tests Automátizados
* ./mvnw verify

### Funcionalidades
1. Autenticação: Basic (usuario: admin senha: 12345)
2. Livros: http://localhost:8080/books/{titulo} - Essa busca retorna da api do google os livros com o titulo especificado
3. Favoritos: http://localhost:8080/books/with-stars GET / para listar favoritos com paginação)
4. Favoritos: http://localhost:8080/books/books/{idGoogle}/favorite (POST / salvar favorito)
5. Favoritos: http://localhost:8080/books/books/{idGoogle}/favorite (DELETE / remover favorito)

* idGoogle = Id do Google retornado na busca pelo titulo do livro
* titulo = titulo do livro na qual deseja buscar

### Observações sobre o projeto
Verifiquei que a vaga nescessitava de alguem com conhecimento no framework Java Quarkus, 
com isso dei uma estudada na documentação e montei o projeto baseado nele.


