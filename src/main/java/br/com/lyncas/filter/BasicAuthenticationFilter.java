package br.com.lyncas.filter;

import javax.inject.Inject;
import javax.ws.rs.WebApplicationException;
import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.core.HttpHeaders;
import javax.ws.rs.core.Response.Status;
import javax.ws.rs.ext.Provider;

import br.com.lyncas.service.AuthService;

@Provider
public class BasicAuthenticationFilter implements ContainerRequestFilter {
	
	@Inject
	AuthService authService;

	@Override
	public void filter(ContainerRequestContext containerRequest) throws WebApplicationException {

		String authCredentials = containerRequest.getHeaderString(HttpHeaders.AUTHORIZATION);

		boolean authStatus = authService.authenticate(authCredentials);

		if (!authStatus) {
			throw new WebApplicationException(Status.UNAUTHORIZED);
		}

	}

}