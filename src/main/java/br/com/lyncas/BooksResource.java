package br.com.lyncas;

import javax.inject.Inject;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.Response.Status;

import br.com.lyncas.bo.GoogleBooksBO;
import br.com.lyncas.model.Book;
import br.com.lyncas.service.BookService;

@Path("/books")
public class BooksResource {
	
	@Inject 
	GoogleBooksBO booksBO;
	
	@Inject
	BookService bookService;
	
	@GET
	@Path("/{title}")
	@Produces(MediaType.APPLICATION_JSON)
	public Response getBooks(@PathParam("title") String title) throws Exception {
		
		try {
			return Response.ok(booksBO.getBooksByTitle(title)).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
		
	}
	
	@POST
	@Path("/{id}/favorite")
	@Produces(MediaType.APPLICATION_JSON)
	public Response favorite(@PathParam("id") String id) throws Exception {
		
		try {
			Book book = bookService.addFavorite(id);
			return Response.ok("Livro: " + book.getTitle() + " foi adicionado com sucesso a lista de favoritos!").build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
		
	}
	
	@DELETE
	@Path("/{id}/favorite")
	@Produces(MediaType.APPLICATION_JSON)
	public Response removeFavorite(@PathParam("id") String id) throws Exception {
		
		try {
			bookService.removeFavorite(id);
			return Response.ok("Livro removido com sucesso a lista de favoritos!").build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
		
	}
	
	@GET
	@Path("/whit-stars")
	@Produces(MediaType.APPLICATION_JSON)
	public Response whitStars() throws Exception {
		
		try {
			return Response.ok(bookService.getBooksWhitStars()).build();
		} catch (Exception e) {
			return Response.status(Status.BAD_REQUEST).entity(e.getMessage()).build();
		}
		
	}
	
}
