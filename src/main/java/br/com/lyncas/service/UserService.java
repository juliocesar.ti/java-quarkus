package br.com.lyncas.service;

import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;

import br.com.lyncas.model.User;

@ApplicationScoped
public class UserService {
	
	@Inject 
	EntityManager entityManager;
	
	public Optional<User> getUser(String login, String password) {
		return entityManager.createQuery("select u from User u where u.login = :login and u.password = :password", User.class)
				.setParameter("password", password)
				.setParameter("login", login)
				.getResultList().stream().findFirst();
	}

}
