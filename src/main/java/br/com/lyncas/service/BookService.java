package br.com.lyncas.service;

import java.util.List;
import java.util.Optional;

import javax.enterprise.context.ApplicationScoped;
import javax.inject.Inject;
import javax.persistence.EntityManager;
import javax.transaction.Transactional;

import br.com.lyncas.bo.GoogleBooksBO;
import br.com.lyncas.model.Book;
import br.com.lyncas.model.StarEnum;

@ApplicationScoped
@Transactional
public class BookService {

	@Inject
	EntityManager entityManager;
	
	@Inject
	GoogleBooksBO googleBooksBO;

	public List<Book> getBooks() {
		return entityManager.createQuery("select b from Book b", Book.class).getResultList();
	}
	
	public List<Book> getBooksWhitStars() {
		return entityManager.createQuery("select b from Book b where b.star = 'Y'", Book.class).getResultList();
	}

	public void addBook(Book v) {
		entityManager.persist(v);
	}

	public Optional<Book> getByIdGoogle(String idGoogle) {
		return entityManager.createQuery("select b from Book b where b.idGoogle = :idGoogle", Book.class)
				.setParameter("idGoogle", idGoogle)
				.getResultList().stream().findFirst(); 
	}
	
	public Book addFavorite(String idGoogle) throws Exception {
		Optional<Book> book = this.getByIdGoogle(idGoogle);
		
		if(!book.isPresent()) {
			Book bookGoogle = googleBooksBO.getGoogleBooksById(idGoogle);
			bookGoogle.setStar(StarEnum.Y);
			addBook(bookGoogle);
			return bookGoogle;
		}
		
		book.get().setStar(StarEnum.Y);
		entityManager.merge(book.get());
		
		return book.get();
	}

	public void removeFavorite(String id) throws Exception {
		Optional<Book> book = this.getByIdGoogle(id);
		
		if(!book.isPresent()) return;
		
		book.get().setStar(StarEnum.N);
		entityManager.merge(book.get());
	}
}
