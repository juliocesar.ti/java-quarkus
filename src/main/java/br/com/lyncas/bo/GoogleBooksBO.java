package br.com.lyncas.bo;

import java.text.NumberFormat;
import java.util.ArrayList;
import java.util.List;

import javax.enterprise.context.ApplicationScoped;

import org.eclipse.microprofile.config.inject.ConfigProperty;

import com.google.api.client.googleapis.javanet.GoogleNetHttpTransport;
import com.google.api.client.json.JsonFactory;
import com.google.api.client.json.jackson2.JacksonFactory;
import com.google.api.services.books.Books;
import com.google.api.services.books.BooksRequestInitializer;
import com.google.api.services.books.model.Volume;
import com.google.api.services.books.model.Volume.SaleInfo;
import com.google.api.services.books.model.Volumes;

import br.com.lyncas.model.Book;

@ApplicationScoped
public class GoogleBooksBO {

	private static final String prefix = "intitle:";
	
	@ConfigProperty(name = "google.api.key")
	private static String API_KEY;
	
	private static final String APPLICATION_NAME = "LYNCAS BOOK";
	private static final NumberFormat CURRENCY_FORMATTER = NumberFormat.getCurrencyInstance();
	
	public List<Book> getBooksByTitle(String title) throws Exception {
		return requestGoogleBooks(title);
	}
	
	public Book getGoogleBooksById(String id) throws Exception {
		return requestGoogbleBookById(id);
	}
	
	private static Book requestGoogbleBookById(String id) throws Exception {
		
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
		
		final Books books = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, null)
				.setApplicationName(APPLICATION_NAME)
				.setGoogleClientRequestInitializer(new BooksRequestInitializer(API_KEY)).build();
		
		Volume volume = books.volumes().get(id).execute();
		
		if (volume.getAccessInfo() == null) throw new Exception("Book not found!");
		
		return volumeToBook(volume);
	}

	private static List<Book> requestGoogleBooks(String title) throws Exception {
		
		JsonFactory jsonFactory = JacksonFactory.getDefaultInstance();
		String query = prefix + title;
		
		final Books books = new Books.Builder(GoogleNetHttpTransport.newTrustedTransport(), jsonFactory, null)
				.setApplicationName(APPLICATION_NAME)
				.setGoogleClientRequestInitializer(new BooksRequestInitializer(API_KEY)).build();
		
		com.google.api.services.books.Books.Volumes.List volumesList = books.volumes().list(query);
		volumesList.setFilter("ebooks");

		//Query
		Volumes volumes = volumesList.execute();
		if (volumes.getTotalItems() == 0 || volumes.getItems() == null) throw new Exception("No matches found");
		
		List<Book> bookList = new ArrayList<Book>();
		
		//Results
		for (Volume volume : volumes.getItems()) {
			bookList.add(volumeToBook(volume));
		}

		return bookList;
	}
	
	public static Book volumeToBook(Volume volume) {
		Book book = new Book();
		
		//Title
		book.setTitle(volume.getVolumeInfo().getTitle());
		//Id
		book.setIdGoogle(volume.getId());
		//Author
		java.util.List<String> authors = volume.getVolumeInfo().getAuthors();
		book.setAuthor(authors != null && !authors.isEmpty() ? authors.get(0) : "");
		//Description
		book.setDescription(getGoogleDescription(volume));
		//Link
		book.setLink(volume.getVolumeInfo().getInfoLink());
		//Price
		book.setPrice(getGooglePrice(volume.getSaleInfo()));
		
		return book;
	}
	
	public static String getGooglePrice(SaleInfo saleInfo) {
		return saleInfo != null && "FOR_SALE".equals(saleInfo.getSaleability()) ? CURRENCY_FORMATTER.format(saleInfo.getRetailPrice().getAmount()) : null;
	}
	
	public static String getGoogleDescription(Volume volume) {
		return volume.getVolumeInfo().getDescription() != null && volume.getVolumeInfo().getDescription().length() > 0 ? volume.getVolumeInfo().getDescription() : "";
	}
}
